/**
  * Created by askellio on 10/20/17.
  */

object Main extends App{

  //размер диапазона
  val bound = 10

  val set5 = singletonSet(5)
  val set6 = singletonSet(6)

  val setUnion = union(set5, set6)

  println("5+6")
  println(contains(setUnion, 5))
  println(contains(setUnion, 6))
  println(contains(setUnion, 7))

  val setFilter = filter(setUnion, (elem: Int) => elem%2 == 0)

  println ("filter elem%2==0")
  println(contains(setFilter, 5))
  println(contains(setFilter, 6))

  val set8 = singletonSet(8)

  println ("+8")
  val setUnionFilter = union(setFilter, set8)

  println(forall(setUnionFilter, (elem:Int) => elem<10))
  println(forall(setUnionFilter, (elem:Int) => elem<7))
  println(exists(setUnionFilter, (elem:Int) => elem<7))

  val setMap = map(setUnionFilter, (elem: Int) => elem-3)

  println ("map -3 before")
  println(contains(setUnionFilter, 5))
  println(contains(setUnionFilter, 6))
  println(contains(setUnionFilter, 7))
  println(contains(setUnionFilter, 8))

  println ("map -3 after")
  println(contains(setMap, 5))
  println(contains(setMap, 6))
  println(contains(setMap, 7))
  println(contains(setMap, 8))

  type Set = Int => Boolean

  def contains(s: Set, elem: Int): Boolean = s(elem)

  //синглтон
  def singletonSet(x: Int): Set = (elem: Int) => elem == x

  //объединение
  def union(s: Set, t: Set): Set = (elem:Int) => s(elem) || t(elem)

  //пересечение
  def intersect(s: Set, t: Set): Set = (elem:Int) => s(elem) && t(elem)

  //разность
  def diff(s: Set, t: Set): Set = (elem:Int) => s(elem) && !t(elem)

  //фильтр
  def filter(s: Set, p: Int => Boolean): Set = (elem:Int) => s(elem) && p(elem)

  //условие выполняется у всех элементов
  def forall(s: Set, p: Int => Boolean): Boolean = {
    def iter(elem: Int): Boolean = {
      if (bound < elem)
        true
      else if (s(elem) && !p(elem)) {
          false
      } else iter(elem+1)
    }
    iter(-1*bound)
  }

  //условие выполняется хотя бы у одного элемента
  def exists(s: Set, p: Int => Boolean): Boolean = {
    def iter(elem: Int): Boolean = {
      if (bound < elem)
        false
      else if (s(elem) && p(elem)) {
        true
      } else iter(elem+1)
    }
    iter(-bound)
  }

  //условие выполняется у всех элементов
  def map(s: Set, f: Int => Int): Set = {
    def iter(elem: Int, set: Set): Set = {
      if (bound < elem)
        set
      else if (s(elem)) {
        iter(elem+1, union(set, (x:Int) => f(elem) == x))
      } else
        iter(elem+1, set)
    }
    iter(-bound, (x: Int) => false)
  }
}
